package id.ub.socialme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telecom.Call;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Detail extends AppCompatActivity {
    TextView tvu, tvs;
    Button btEdit, logout, delete;
    String userd, idd, statusd, idus, sus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        tvu = findViewById(R.id.tvUsernameD);
        tvs = findViewById(R.id.tvStatusD);

        Intent i = getIntent();
        userd = i.getStringExtra("username");
        statusd = i.getStringExtra("status");
        idd = i.getStringExtra("id");
        tvu.setText(userd);
        tvs.setText(statusd);
        btEdit = findViewById(R.id.btEdit);
        delete = findViewById(R.id.btDelete);

        FirebaseDatabase fbdbu = FirebaseDatabase.getInstance();
        DatabaseReference refu = fbdbu.getReference().child("user");
        refu.orderByChild("username").equalTo(userd).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ss : snapshot.getChildren()) {
                    idus = ss.getKey();

                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (idd.equals(idus)) {
                                DatabaseReference refs = fbdbu.getReference().child("status");
                                refs.orderByChild("status").equalTo(statusd).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        for (DataSnapshot ss: snapshot.getChildren()) {
                                            ss.getRef().removeValue();
                                            Intent dl = new Intent(Detail.this, Home.class);
                                            startActivity(dl);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });

                            }
                            else {
                                Toast.makeText(Detail.this, "You cannot delete other user status.", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    btEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (idd.equals(idus)) {
                                DatabaseReference refs = fbdbu.getReference().child("status");
                                refs.orderByChild("status").equalTo(statusd).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        for (DataSnapshot ss: snapshot.getChildren()) {
                                            sus = ss.getKey();
                                            Intent ed = new Intent(Detail.this, EditPost.class);
                                            ed.putExtra("username", userd);
                                            ed.putExtra("status", statusd);
                                            ed.putExtra("idstatus", sus);
                                            ed.putExtra("iduser", idd);
                                            startActivity(ed);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });

                            }
                            else {
                                Toast.makeText(Detail.this, "You cannot edit other user status.", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        logout = findViewById(R.id.btLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btLogout) {
                    FirebaseAuth.getInstance().signOut();
                    Intent so = new Intent(Detail.this, SignIn.class);
                    startActivity(so);
                }
            }
        });

    }
}