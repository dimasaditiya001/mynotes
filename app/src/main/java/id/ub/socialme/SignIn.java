package id.ub.socialme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SignIn extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    EditText email, pass;
    Button si;
    TextView regist;
    FirebaseDatabase fbdbu = FirebaseDatabase.getInstance();
    DatabaseReference refRoot = fbdbu.getReference();
    DatabaseReference refUser = refRoot.child("user");
    User u = new User();
    ArrayList<String> info = new ArrayList<String>();
    String id2, user2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        email = findViewById(R.id.etEmail);
        pass = findViewById(R.id.etPassword);
        si = findViewById(R.id.btSI);
        regist = findViewById(R.id.tvRegist);

        String text = "Don't have an account? Sign Up";
        SpannableString ss = new SpannableString(text);

        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent su = new Intent(SignIn.this, SignUp.class);
                startActivity(su);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.WHITE);
            }
        };


        ss.setSpan(cs, 23, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        regist.setText(ss);
        regist.setMovementMethod(LinkMovementMethod.getInstance());

        mAuth = FirebaseAuth.getInstance();

        si.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btSI) {
            if (email.getText().toString().isEmpty() && pass.getText().toString().isEmpty()) {
                Toast.makeText(SignIn.this, "Enter your email and password.", Toast.LENGTH_LONG).show();
            } else if (email.getText().toString().isEmpty()) {
                Toast.makeText(SignIn.this, "Enter your email.", Toast.LENGTH_LONG).show();
            } else if (pass.getText().toString().isEmpty()) {
                Toast.makeText(SignIn.this, "Enter your password.", Toast.LENGTH_LONG).show();
            } else {
                mAuth.signInWithEmailAndPassword(email.getText().toString(), pass.getText().toString()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            FirebaseUser user = mAuth.getCurrentUser();
                                            if (user != null) {
                                                if (user.isEmailVerified()) {
                                                    Intent home = new Intent(SignIn.this, Home.class);
                                                    startActivity(home);
                                                } else {
                                                    Toast.makeText(SignIn.this, "Not verified", Toast.LENGTH_LONG).show();

                                                }
                                            }

                                        } else {
                                            Toast.makeText(SignIn.this, "Authentication failed.", Toast.LENGTH_LONG).show();
                                        }

                                    }
                                }

                        );

            }
        }


    }
}