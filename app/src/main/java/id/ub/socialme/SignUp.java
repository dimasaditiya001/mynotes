package id.ub.socialme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUp extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    EditText email, pass, uname;
    Button su;
    TextView login;
    FirebaseDatabase fbdbu = FirebaseDatabase.getInstance();
    DatabaseReference refRoot = fbdbu.getReference();
    DatabaseReference refUser = refRoot.child("user");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        email = findViewById(R.id.etEmailr);
        pass = findViewById(R.id.etPasswordr);
        uname = findViewById(R.id.etUserr);
        su = findViewById(R.id.btSU);
        login = findViewById(R.id.tvLogin);

        String text = "Already have an account? Sign In";
        SpannableString ss = new SpannableString(text);

        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent su = new Intent(SignUp.this, SignIn.class);
                startActivity(su);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.WHITE);
            }
        };

        ss.setSpan(cs, 25, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        login.setText(ss);
        login.setMovementMethod(LinkMovementMethod.getInstance());

        mAuth = FirebaseAuth.getInstance();


        su.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btSU) {
                if (email.getText().toString().isEmpty() && uname.getText().toString().isEmpty() && pass.getText().toString().isEmpty()) {
                    Toast.makeText(SignUp.this, "Complete your form.", Toast.LENGTH_LONG).show();
                }
                else if (email.getText().toString().isEmpty()) {
                    Toast.makeText(SignUp.this, "Enter your email.", Toast.LENGTH_LONG).show();
                }
                else if (uname.getText().toString().isEmpty()) {
                    Toast.makeText(SignUp.this, "Enter your username.", Toast.LENGTH_LONG).show();
                }
                else if (pass.getText().toString().isEmpty()) {
                    Toast.makeText(SignUp.this, "Enter your password.", Toast.LENGTH_LONG).show();
                }
                else {
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                            .addOnCompleteListener(this,
                                    new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                FirebaseUser user = mAuth.getCurrentUser();
                                                if (user != null) {

                                                    //Kasih nama, jika menggunakan auth google tidak perlu krena sdah ada nama di googlenya
                                                    //nama bisa diberikan textfield sendiri agar user ketika registrasi masukan nama user tersebut
                                                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                            .setDisplayName(uname.getText().toString()) //contoh user test namanya
                                                            .build();

                                                    user.updateProfile(profileUpdates)
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        Log.d("gagal update profil", "User profile updated.");
                                                                    }
                                                                }
                                                            });
                                                    /////////////////////////////////////////

                                                    if (user.isEmailVerified()) {   //cek sudah di verifikasi di emailnya apa belum
                                                        Intent home = new Intent(SignUp.this, Home.class);
                                                        home.putExtra("email", email.getText().toString());
                                                        startActivity(home);
                                                    } else {

                                                        final String emails = user.getEmail();

                                                        user.sendEmailVerification().addOnCompleteListener(SignUp.this, new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    Toast.makeText(SignUp.this, "Verification email sent to " + emails, Toast.LENGTH_SHORT).show();
                                                                    String unames = uname.getText().toString();
                                                                    String emails = email.getText().toString();
                                                                    String passs = pass.getText().toString();
                                                                    User u = new User();
                                                                    u.setUsername(unames);
                                                                    u.setEmail(emails);
                                                                    u.setPassword(passs);
                                                                    String key = refUser.push().getKey();
                                                                    u.setIdUser(key);
                                                                    refUser.child(key).setValue(u);
                                                                } else {
                                                                    Log.e("Error di verifikasi", "sendEmailVerification", task.getException());
                                                                    Toast.makeText(SignUp.this, "Failed to send verification email.", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });

                                                    }
                                                }
                                            } else {
                                                Toast.makeText(SignUp.this, "Authentication Failed", Toast.LENGTH_LONG).show();
                                            }

                                        }
                                    }

                            );
                }

            }

        }

    }